package pl.edu.pwsztar;

public class Main {

    public static void main(String[] args) {
        ListCollection listCollection = new ListCollection();

        for (TraversalAbstraction iter = listCollection.createIterator(); iter.hasNext();)
        {
            String name = (String)iter.next();
            System.out.println("Name : " + name);
        }
    }
}
