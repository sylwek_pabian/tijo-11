package pl.edu.pwsztar;

public class ListTraversal implements TraversalAbstraction {
    public String[] names = new String[]{"Robert", "John", "Julie", "Lora"};

    private int index;

    public boolean hasNext() {
        if (index < names.length) {
            return true;
        }
        return false;
    }

    public Object next() {
        if (hasNext()) {
            return names[index++];
        }
        return null;
    }
}