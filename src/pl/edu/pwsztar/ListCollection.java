package pl.edu.pwsztar;

public class ListCollection implements Collection {
    public TraversalAbstraction createIterator() {
        return new ListTraversal();
    }
}