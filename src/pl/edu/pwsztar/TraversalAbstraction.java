package pl.edu.pwsztar;

public interface TraversalAbstraction {
    boolean hasNext();
    Object next();
}