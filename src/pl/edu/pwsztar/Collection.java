package pl.edu.pwsztar;

public interface Collection {
    TraversalAbstraction createIterator();
}