# Iterator (wzorzec projektowy)

**Iterator** – czynnościowy wzorzec projektowy (obiektowy), którego celem jest zapewnienie sekwencyjnego dostępu do 
podobiektów zgrupowanych w większym obiekcie.

## Przykłady zastosowania

Ze wzorca Iterator należy korzystać w następujących warunkach:

* Kiedy chcesz uzyskać dostęp do zawartości obiektu zagregowanego bez ujawniania jego wewnętrznej reprezentacji
* Jeśli chcesz umożliwić jednoczesne działanie wielu procesów, przechodzenia po obiektach zagregowanych
* Jeżeli chcesz udostępnić jednolity interfejs do poruszania się po różnych zagregowanych strukturach (czyli zapewnić 
obsługę iteracji polimorficznej)

## Struktura wzorca

![alt text](http://devman.pl/img/Iterator.png)

W dziale Struktura mamy diagram UML, który składa się z:

Klienta, który korzysta z klas ListCollection oraz MapCollection, które to dziedzieczą po klasie abstrakcyjnej 
Collection, która definiuje metodę createTraversalObject(), metoda createTraversalObject() tworzy klasę ListTraversal 
a klasa ListTraversal dziedziczy po interfejsie TraversalAbstraction, który ma zdefiniowane metody iterujące po kolekcji.

## Konsekwencje stosowania
Do konsekwencji stosowania wzorca należy możliwość zapewnienia różnych sposobów iterowania obiektu.

## Powiązania z innymi wzorcami

* Iterator może iterować przez gałęzie kompozytu. Visitor może zaimplementowac operacje w kompozycie.
* Tzw iteratory poliformiczne opierają się o metody Fabryki, żeby zaimplementować odpwiednią podklasę iteratora.
* Iterator może być użyty razem ze wzorcem Memento, aby uchwycić stan iteracji.


## Źródła
* Źródło1 [Wikipedia](https://pl.wikipedia.org/wiki/Iterator_(wzorzec_projektowy)) 
* Źródło2 [devman.pl](http://devman.pl/pl/techniki/wzorce/wzr-opr/wzorce-projektowe-iteratoriterator/#Struktura) 